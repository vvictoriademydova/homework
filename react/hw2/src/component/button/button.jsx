import { Component } from "react";

class Button extends Component{
    constructor(props){
        super(props);
        this.state = {
            openedModalId: ""
        }
    }
        render() {
          const { text, onClick, id,className,svg } = this.props;
          return (
            <button className={className} onClick={onClick} id={id}>
          <p>{text}</p>
            </button>
          );
        }
      
}

export default Button