import { Component } from "react";
import Car from "./car.jsx"

export default class CardsCar extends Component {
  constructor(props){
    super(props)
  }
    render() {
        return (
          <div >
            {this.props.cars.map((car) => (
              <Car
                car={car}
                
              />
            ))}
          </div>
        );
      }
    
}