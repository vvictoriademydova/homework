import logo from './logo.svg';
import './App.css';
import ConteinerButton from "./component/conteinerButton"
import { Component } from "react";

import CardsCar from "./component/cards/cards.jsx"


class App extends Component {
  constructor() {
    super();
    this.state = {
      cars: [],
    }}

    componentDidMount() {
      fetch("items.json")
        .then((response) => response.json())
        .then((cars) => 
        this.setState({ cars: cars})
        );

  }
  


render(){
  return  <CardsCar
  cars={this.state.cars}

/>
}
}

export default App;
