import { Component } from "react";
import Button from "../button/button.jsx";



class Modal extends Component {
    constructor(props) {
      super(props);
      this.modalWindowDeclarations = [
        {
          id: "modal1",
          title: "title for modal 1",
          action: () => {},
          description: "description for modal 1",
        },
        {
          id: "modal2",
          title: "title for modal 2",
          description: "description for modal 2",
        },
      ];
      this.state = {
        modal: {},
      };
    }
    componentDidMount(e) {
      const modalToShow = this.modalWindowDeclarations.find(
        (modal) => modal.id === this.props.openedModal
      );
      this.setState({ modal: modalToShow });
    }
    render() {
      return (
        <>
        <div className="modal-background"
          onClick={this.props.closeModal}>
              </div>
        <div className="modal-window">
            {this.state.modal.title ? <h2>{this.state.modal.title}</h2> : null}
              <p>{this.state.modal.description}</p>
          <Button
           onClick={this.props.closeModal}
           text="Ok"
           id="closeModal"
           className="buttonClose"
          />
            </div>
        </>
        
      );
    }
  }
  
  export default Modal;
