import { Component } from "react";

class Button extends Component{
    constructor(props){
        super(props);
        this.state = {
            openedModalId: ""
        }
    }
        render() {
          const { text, backgroundColor, onClick, id,className } = this.props;
          return (
            <button className={className} onClick={onClick} id={id}>
              {text}
            </button>
          );
        }
      
}

export default Button