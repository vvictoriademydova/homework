import { Component } from "react";
import Button from "../button/button.jsx";
import Modal from "../modalWindow/modal.jsx"


class ConteinerButton extends Component {
  constructor(props) {
    super(props);
    this.modalBackground = document.querySelector(".modal-backgroun")
    this.state = {
      openedModalId: "",
    };
  }

 
  openModal = (modalId) => {
    this.setState({ openedModalId: modalId });
  };
  closeModal = (e) => {
    this.setState({ openedModalId: "" });
   
  };

  modalButtonClickHandler = (e)=>{
    console.log(e.currentTarget.id);
    const modalId = e.currentTarget.id
      ? e.currentTarget.id
      : "";
    this.setState({ openedModalId: modalId });
  }

  
  render() {
    return (
      <div>
        <div>

          <Button
          onClick={this.modalButtonClickHandler}
          text="open1"
          id="modal1"
          className="buttonOpen"
          />

          <Button
          onClick={this.modalButtonClickHandler}
          text="open2"
          id="modal2"
          className="buttonOpen"
          />

          {this.state.openedModalId ? (
            <Modal
              openedModal={this.state.openedModalId}
              closeModal={this.closeModal}
            />
          ) : null}
        </div>
      </div>
    );
  }
}

export default ConteinerButton;

