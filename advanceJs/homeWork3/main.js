
//1завдання
let clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
let clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsAll= [...clients1]
clients2.forEach(item=>{
  if(!clientsAll.includes(item)){
    clientsAll.push(item)
  }
})

console.log(clientsAll)



//2 завдання
const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];
  

  const nameLastAge = () =>{
    const arrayData = [];
    function personsData({name, lastName, age}){
            return {name, lastName, age}
        }
    characters.forEach(item=>{
        arrayData.push(personsData(item))
        
    })
    console.log(arrayData);
  }
  nameLastAge();
  

//3 завдання

const user1 = {
        name: "John",
        years: 30, 
      };
    
    const {name, years, isAdmin=false} = user1;
    console.log(name);
    console.log(years);
    console.log(isAdmin)
    


//4 завдання
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
  }
  
  const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
  }
  
  const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
  }
const fullProfile={...satoshi2018, ...satoshi2019, ...satoshi2020}

console.log(fullProfile)



//5 завдання
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const addNewBook = {
    name: ' Zoo Station: The Story of Christiane F.',
    author: 'Christiane F.'
  }

  const booksAll = [...books, addNewBook]
  console.log(booksAll)
  console.log(books)


//6 завдання
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}
const newEmployee = {...employee, age:25, salary:10000}
console.log(newEmployee)

// 7 завдання
const array = ['value', () => 'showValue'];
const [value, showValue] = array;

console.log(value);
console.log(showValue(array));

