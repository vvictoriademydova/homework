const container = document.querySelector(".container");
fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((res) => res.json())
  .then((data) => {
    const wrapper = document.createElement("div");
    wrapper.classList.add("wrapper");
    let charactersArr = [];
    data.forEach(({ episodeId, name, openingCrawl, characters }, index) => {
      const card = document.createElement("div");
      const ul = document.createElement("ul");
      ul.innerHTML =
        '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>';
      ul.classList.add("story", `story_${index}`);
      card.append(ul);
      card.classList.add("card");
      card.insertAdjacentHTML(
        "afterbegin",
        `
        <h2 class="eposideId">Episod of the movie - ${episodeId}</h2>
        <h3 class="movieName">${name}</h3>
        <div class="description">
          ${openingCrawl}
        </div>
      `
      );
      wrapper.append(card);
      charactersArr.push(characters);
    });
    container.append(wrapper);
    return charactersArr;
  })
  .then((data) => {
    data.forEach((el, index) => {
      return Promise.all(
        el.map((el) =>

          fetch(el)

            .then((res) => res.json())

            .then(({ name }) => `<li>${name}</li>`)

        )

      ).then((data) => {

        const container = document.querySelector(`.story_${index}`);

        container.innerHTML = data.join("");

      });

    });

  });
