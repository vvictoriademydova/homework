// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.


class Employee {
    constructor() {
        this._name = null;
        this._age = null;
        this._salary = null;
    }
        set name (value) {
            if (value.length > 2) {
                this._name = value
            } else {
                return 'error';
            }
        }
        get name () {
            return this._name
        }


        set age(value){
            this._age = value
        }
        get age () {
            return this._age
        }

        set salary(value){
            this._salary = value
        }
        get salary(){
            return this._salary
        }

      
}

// const user = new Employee();
// user.name = "Victoria";
// user.age =  "27";
// user.salary = 10000;
// console.log(user);


class Programmer extends Employee{
    constructor(){
        super()
        this._lang = null;
    }
    set lang(value){
        this._lang = value
    }

    get lang(){
        return this._lang
    }
    set salary(value){
        this._salary = value
    }
    get salary(){
            return this._salary * 3  
        
    }


}
// const user1 = new Programmer();
// user1.name = "Victoria";
// user1.age =  "27";
// user1.lang = "english";
// user1.salary = 10000;
// console.log(user1);


// const user2 = new Programmer();
// user2.name = "Milana";
// user2.age =  "20";
// user2.lang = "spanish";
// user2.salary = 40000;
// console.log(user2);