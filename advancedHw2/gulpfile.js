
const {src, dest, watch, parallel, series} = require("gulp");
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const debug = require("gulp-debug");
const notify = require("gulp-notify");
const plumber = require('gulp-plumber');
let rename = require("gulp-rename");
let uglify = require('gulp-uglify-es').default;
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();
const del = require('del');
const clean = require('gulp-clean');
const fileinclude = require('gulp-file-include');

function serve() {
	browserSync.init({
		server: {
			baseDir: "dist/"
		}
	});
}
function buildStyles() {
	return src('./src/scss/**/main.scss')
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(debug({title: 'Src'}))
		.pipe(sass().on('error', sass.logError))
		.pipe(debug({title: 'Css'}))
		.pipe(autoprefixer({
			cascade: false
		}))
		.pipe(concat('styles.min.css'))
		.pipe(cleanCSS())
		.pipe(debug({title: 'Concat'}))
		.pipe(sourcemaps.write())
		.pipe(dest('./dist/css'))
		.pipe(browserSync.stream());
}

function buildScripts() {
	return src('./src/js/**/*.js')
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(debug({title: 'Src'}))
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(debug({title: 'Concat'}))
		.pipe(sourcemaps.write())
		.pipe(dest('./dist/js'))
		.pipe(browserSync.stream());
}
function buildPages() {
	return src('./src/index.html')
	.pipe(fileinclude())
		.pipe(dest('./dist'))
		.pipe(browserSync.stream());
}
function copyImg() {
	return src('./src/img/**/*.*')
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.mozjpeg({quality: 75, progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
				plugins: [
					{removeViewBox: true},
					{cleanupIDs: false}
				]
			})
		]))
		.pipe(dest('./dist/img'))
		.pipe(browserSync.stream());
}

function cleanDist() {
	return del('dist')
}
function watching() {
	watch(['./src/scss/**/*.scss'], buildStyles)	
	watch(['./src/html/*.html'], buildPages)
	watch(['./src/js/**/*.js'], buildScripts)
}

exports.buildStyles = buildStyles;
exports.buildScripts = buildScripts;
exports.buildPages = buildPages;
exports.copyImg = copyImg;
exports.cleanDist = cleanDist;
exports.serve = serve;
exports.watching = watching;

exports.build = series(cleanDist, parallel(buildPages, copyImg, buildStyles, buildScripts,serve, watching))
