let images = document.querySelectorAll(".images-wrapper");
let stopBtn = document.querySelector(".stop");
let show = document.querySelector(".show");
let currentImage = 0;
console.log(images);

let slideShow = ()=>{
    images[0].children[currentImage].classList.add("hide");
    if(currentImage === images[0].children.length-1){
        currentImage = 0;  
    }else{
        currentImage++
    }
    images[0].children[currentImage].classList.remove("hide");
}

let intervalShow = setInterval(slideShow, 3000);

show.addEventListener("click", ()=>{
   intervalShow = setInterval(slideShow, 3000);
})

stopBtn.addEventListener("click", () => {
    clearInterval(intervalShow)
})
