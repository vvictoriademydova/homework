// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let paragraphs = document.querySelectorAll('p');
paragraphs.forEach(item => {
    item.style.backgroundColor = '#ff0000';
});

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// const optionsList = document.querySelector('#optionsList')
// console.log(optionsList);
// console.log(optionsList.parentNode);

const optionsListChildren = optionsList.childNodes;
console.log(optionsListChildren);
console.log(typeof optionsListChildren);

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph
let testParagraph = document.querySelector('#testParagraph');
testParagraph.innerText = `This is a paragraph`


// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

let mainHeader = document.querySelector('.main-header');
console.log(mainHeader);

let mainHeaderChildren = mainHeader.children
console.log(mainHeaderChildren);
for (const elem of mainHeaderChildren) {
        elem.classList.add("nav-item");
    }
    console.log(mainHeaderChildren);



// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
// const elemSectionTitle = document.getElementsByClassName(`section-title`);
// console.log(elemSectionTitle);
// такого класу нема, я візьму header-title

const elemHeaderTitle = document.querySelectorAll(`.header-title`);
elemHeaderTitle.forEach(item => {
    item.classList.remove('header-title');
});
console.log(elemHeaderTitle);
