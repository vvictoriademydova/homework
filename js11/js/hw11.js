
const passShowHide = () =>{
    let pass = document.querySelector('#password-input');
    let confirmPass = document.querySelector('#password-input-confirm')
    let iconPass = document.querySelectorAll('.icon-password');
    let iconPassConfirm = document.querySelectorAll('.icon-password-confirm');
    const button = document.querySelector('button');

    
    const icon = document.querySelectorAll('.fas')
    icon.forEach((item)=>{
            if(item.classList.contains('fa-eye-slash')){
                item.classList.remove('fa-eye-slash');
            }
        })
            
            iconPass.forEach((item)=>{
                item.addEventListener('click', ()=>{
                    if(item.classList.contains('fa-eye')){
                        item.classList.remove('fa-eye');
                        item.classList.add('fa-eye-slash');
                        pass.setAttribute('type', 'text');
                    }else if(item.classList.contains('fa-eye-slash')){
                        item.classList.remove('fa-eye-slash');
                        item.classList.add('fa-eye');
                        pass.setAttribute('type', 'password');
                    }

                })
            });

            iconPassConfirm.forEach((item)=>{
                item.addEventListener('click', ()=>{
                    if(item.classList.contains('fa-eye')){
                        item.classList.remove('fa-eye');
                        item.classList.add('fa-eye-slash');
                        confirmPass.setAttribute('type', 'text');
                    }else if(item.classList.contains('fa-eye-slash')){
                        item.classList.remove('fa-eye-slash');
                        item.classList.add('fa-eye');
                        confirmPass.setAttribute('type', 'password');
                    }

                })
            });
    button.addEventListener('click',(event)=>{
        if(pass.value !== confirmPass.value){
            alert('Потрібно ввести однакові значення')
        } else if(pass.value === ''){
            alert('Введіть пароль')
        } else {
            alert('Ласкаво просимо');
        }
        event.preventDefault();
       
    }
    
    )
}
passShowHide();

