let tab = ()=>{
    let tabs = document.querySelectorAll(".service-item"),
    tabContent = document.querySelectorAll(".service-tab-item"),
    tabName;
    tabs.forEach(item=>{
        item.addEventListener("click", selectTabs)
    })
    function selectTabs(){
        tabs.forEach(item=>{
            item.classList.remove("active-tab");
        })
        this.classList.add("active-tab");
        tabName = this.getAttribute("data-tab");
        selectTabContent(tabName);
    }
    function selectTabContent(tabName){
        tabContent.forEach(item=>{
            item.classList.contains(tabName)? item.classList.add("service-tab-active"):item.classList.remove("service-tab-active");
        })
    }
};
tab();

function Gallery() {
	const images = [
		{
			data: 'graphic_design',
			src: './img/graphic design/graphic-design1.jpg'
		},{
			data: 'web_design',
			src: './img/graphic design/graphic-design2.jpg'
		},{
			data: 'landing_pages',
			src: './img/graphic design/graphic-design3.jpg'
		},{
			data: 'wordpress',
			src: './img/graphic design/graphic-design4.jpg'
		},{
			data: 'graphic_design',
			src: './img/graphic design/graphic-design5.jpg'
		},{
			data: 'web_design',
			src: './img/graphic design/graphic-design6.jpg'
		},{
			data: 'landing_pages',
			src: './img/graphic design/graphic-design7.jpg'
		},{
			data: 'wordpress',
			src: './img/graphic design/graphic-design8.jpg'
		},{
			data: 'graphic_design',
			src: './img/graphic design/graphic-design9.jpg'
		},{
			data: 'web_design',
			src: './img/graphic design/graphic-design10.jpg'
		},{
			data: 'landing_pages',
			src: './img/graphic design/graphic-design11.jpg'
		},{
			data: 'wordpress',
			src: './img/graphic design/graphic-design12.jpg'
		}
	]
    const galleryWrapper = document.querySelector(".work-list");
    const buttonLoadMore = document.querySelector(".load_more_work");
    const galleryMore = document.querySelector(".work-images");
    galleryWrapper.addEventListener("click",(event)=>{
        let dataMenu = null;
        const dataGallery = document.querySelectorAll(".image-item");
        if (event.target.nodeName === 'BUTTON') {
        dataMenu=event.target.getAttribute("data-menu")
        }

        let menuItems = document.querySelectorAll("[data-menu]");
        menuItems.forEach(item =>{
            if(item.classList.contains("active-tab-filter")){
                item.classList.remove("active-tab-filter")
            }
        })

        if(!event.target.classList.contains("active-tab-filter")){
            event.target.classList.add("active-tab-filter")
        }


        dataGallery.forEach((item)=>{
            if (dataMenu !== item.getAttribute("data-gallery") && dataMenu !== "all"){
                item.style.display = 'none';
            } else {
                item.style.display = 'block';
            }

        })
    })
    let count = 0;
    buttonLoadMore.addEventListener("click",()=>{
        let loadMoreAnimation = document.querySelector(".img-more");
        loadMoreAnimation.classList.add("loader");
         setTimeout(()=>{
            count++;
            loadMoreAnimation.classList.remove("loader");
            images.forEach((image, id)=>{
                galleryMore.insertAdjacentHTML("beforeend",` <div class="image-item work-image-item" data-gallery="${image.data}">
                <img class="img${id+13}" src="${image.src}" alt="image">
				<div class = "creative-design-box">
				<div class = "btn-design">
					<a class = "creative-design-link"></a>
					<a class = "creative-design-square"><svg width="12" height="11" viewBox="0 0 12 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="12" height="11" fill="white"/>
                        </svg>
                        </a>
				</div>
				<h2 class = "creative-design-h">CREATIVE DESIGN</h2>
				<span class = "creative-design-span">Web Design</span>
			</div>
            </div>`)
            })
            if(count === 2){
                buttonLoadMore.remove();
            }
         }, 2000)
    })
}
document.addEventListener('DOMContentLoaded', () => {
	Gallery();
});

let iconUzers = document.querySelectorAll(".user-item-reviews");
	let iconUzer = document.querySelectorAll(".wrapper-user-reviews");
	let userText = document.querySelectorAll(".user-reviews-text");
let activeReviews = ()=>{
	

	iconUzers.forEach(item=>{
		item.addEventListener("click", activeIcon)
	})
	function activeIcon(){
        iconUzers.forEach(item=>{
            item.classList.remove("icon-active");
        })
        this.classList.add("icon-active");
        activeIconUsers = this.getAttribute("data-name");
        selectContent(activeIconUsers);
    }
    function selectContent(activeIconUsers){
        iconUzer.forEach(item=>{
            item.classList.contains(activeIconUsers)? item.classList.add("wrapper-user-reviews-active"):item.classList.remove("wrapper-user-reviews-active");
        })
		userText.forEach(item=>{
            item.classList.contains(activeIconUsers)? item.classList.add("active-text"):item.classList.remove("active-text");
        })
    }
	let arrowNext = document.querySelector(".arrow-next");
	let arrowPrev = document.querySelector(".arrow-back");
		let index = 0;
		let activeI = n =>{
			for(item of iconUzers){
				item.classList.remove("icon-active");
			}
			iconUzers[n].classList.add("icon-active");
			let iconAtribut = iconUzers[n].getAttribute("data-name");
			iconUzer.forEach(item=>{
				item.classList.contains(iconAtribut)? item.classList.add("wrapper-user-reviews-active"):item.classList.remove("wrapper-user-reviews-active");
			})
			userText.forEach(item=>{
				item.classList.contains(iconAtribut)? item.classList.add("active-text"):item.classList.remove("active-text");
			})

		}
		let nextIcon = () =>{
			if(index == iconUzers.length -1){
				index = 0;
				activeI(index)
			} else{
				index++;
				activeI(index)
			}
		}

		let prevtIcon = () =>{
			if(index == 0){
				index = iconUzers.length -1
				activeI(index)
			} else{
				index--;
				activeI(index)
			}
		}
		arrowNext.addEventListener("click", nextIcon)
		arrowPrev.addEventListener("click", prevtIcon)
}
activeReviews()

